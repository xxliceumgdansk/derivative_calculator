#ifndef ENTRYCLASS_H
#define ENTRYCLASS_H

#include<vector>
#include<string>

using namespace std;

struct funct
{
    string function;
    int token;
};

class EntryClass
{
    public:
        vector<funct> functions;
        EntryClass(string entry);
        virtual ~EntryClass();
    protected:
    private:
        string entry;
        void parseOnAddition();
};

#endif // ENTRYCLASS_H

