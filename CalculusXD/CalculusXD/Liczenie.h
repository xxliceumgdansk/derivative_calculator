#ifndef LICZENIE_H
#define LICZENIE_H


#include <iostream>
#include <string>
#include <cmath>
#include <sstream>

using namespace std;

class Liczenie
{
private:
	long long coefficient;
	string function;
	string type;
public:
	Liczenie(string x, string y, long long coeff);
	string monomial();
	string trigonometry();
	string root(string function);
	string rational(string function);
	string exponential_e_based(string function);
	string logarithm_e_based(string function);
	string exponential_a_based(string function);
	string logarithm_a_based(string function);
	
	//empty space for composition of functions???
	
	//string multi(string function);
	//string division(string function);								pozdrawiam, pierdolę, nie robię, pcpc -.-.-	
};

#endif 
