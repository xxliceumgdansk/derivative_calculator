#include "stdafx.h"
#include "EntryClass.h"

using namespace std;

EntryClass::EntryClass(string entry)
{
    this->entry = entry;
    parseOnAddition();
}

// sprawdzaj czy plusy i minusy nie sa przypadkiem w nawiasach, wywal do tokena mnozenie

void EntryClass::parseOnAddition()
{
    bool endOfNumbers;
    int k=0;
    funct temp;
    temp.function = "";
    temp.token = 0;
    string tempToken;
    int tokenValue = 0;
    int parenthesisNumber = 0;

    for(int i=0; i<entry.length(); i++) //leci po calym wejsciu
    {
        if(entry[i]=='(')
            parenthesisNumber++;
        else if(entry[i]==')')
            parenthesisNumber--;
        else if((entry[i]=='+' || entry[i]=='-' || i==(entry.length()-1)) && parenthesisNumber == 0) //dzieli na znakach +/- i ostatyni kawalek
        {
            endOfNumbers=false;     //przy nowym ciagu moze zaczynac sie od nowych cyfr

            if(k=0 && i>0)
                temp.token=1; //sprawdz znak poczatkowy
            else if(entry[k-1]=='-') //dla znaku ustawia odpowiednio 1 lub -1
                temp.token=-1;
            else if(entry[k-1]=='+')
                temp.token=1;

            for(int j=k; j<i; j++) // wpisuje do tymczasowej tablicy, od konca ostgatniego kawalka do poczatku nastepnego
            {
                if((int)entry[i] <= 57  && (int)entry[i] >= 48 && endOfNumbers==false) //jezeli znaki na poczatku sa cyframi to wrzuca je do tokena (kod ascii)
                    tempToken+=entry[i]; //Token tymczasowy do wrzucenia do tymczasowego funct.tokena jako stringa
                else
                    {
                        temp.function+=entry[j]; // reszte funkcyjna daje do podzielonej funkcji
                        endOfNumbers=true; //koniec pierwsdzych liczb
                    }
            }
            for(int j=tempToken.length()-1; j>=0; j--) 
           	{
           		tokenValue+=((tempToken.length()-j-1)*10)*((int)tempToken.length()-48); //przerobienie stringa z tempTokena na inta do tokenValue
           	}
           	temp.token*=tokenValue;
            functions.push_back(temp); //tymczasowa tablice wrzuca do wektora z kawalkami wejscia
            k=i+1; //ustwia poczatek nastepnego kawalka
        }

    }

    return;
}

EntryClass::~EntryClass()
{

}
